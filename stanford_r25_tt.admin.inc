<?php

// standard drupal-type administration forms


// enter pta number form
function _stanford_r25_pta($form, &$form_state) {

  // start with an instructive string
  $markup_str = 'new  .<br />  text here ';

  $form['description'] = array(
    '#markup' => t($markup_str),
  );



  // PTA numbers that needs approval
  $form['stanford_r25_room_pta_numbers'] = array(
    '#type' => 'textfield',
    '#title' => t('PTA numbers '),
    '#description' => t('Enter PTA numbers'),
    '#default_value' => variable_get('stanford_r25_room_pta_numbers', 'Do we have default PTA?'),
    '#required' => FALSE,
  );




  // submit button
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}
